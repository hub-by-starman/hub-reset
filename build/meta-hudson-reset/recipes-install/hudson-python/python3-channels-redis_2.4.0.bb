SUMMARY = "channels-redis Python Module"
LICENSE = "BSD"
DESCRIPTION = "Redis-backed ASGI channel layer implementation"
LIC_FILES_CHKSUM = "file://LICENSE;md5=f09eb47206614a4954c51db8a94840fa"
PYPI_PACKAGE = "channels_redis"
inherit setuptools3 pypi
SRC_URI[md5sum] = "d060c96cc53c6b2c3f93df4d7aefecf3"
SRC_URI[sha256sum] = "b7ccbcb2fd4e568c08c147891b26db59aa25d88b65af826ce7f70c815cfb91bc"

RDEPENDS_${PN} += " \
    python3-msgpack \
"
