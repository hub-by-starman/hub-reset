SUMMARY = "Httptools Python Module"
LICENSE = "MIT"
DESCRIPTION = "httptools is a Python binding for nodejs HTTP parser"
LIC_FILES_CHKSUM = "file://LICENSE;md5=0a2d82955bf3facdf04cb882655e840e"
PYPI_PACKAGE = "httptools"
inherit setuptools3 pypi
SRC_URI[md5sum] = "802a2808a5a7a41f44bc8aa7a6d2465d"
SRC_URI[sha256sum] = "e00cbd7ba01ff748e494248183abc6e153f49181169d8a3d41bb49132ca01dfc"
