SUMMARY = "Asgiref Python Module"
LICENSE = "BSD"
DESCRIPTION = "ASGI in-memory channel layer"
LIC_FILES_CHKSUM = "file://LICENSE;md5=f09eb47206614a4954c51db8a94840fa"
PYPI_PACKAGE = "asgiref"
inherit setuptools3 pypi
SRC_URI[md5sum] = "7f6251f3d9778b3d2e64cac9d2f6b027"
SRC_URI[sha256sum] = "fa83f1bb89020d9e7c8361a41e8a8ccbfcf3b9df2e9e47a320bdc4821a9c3dcd"
