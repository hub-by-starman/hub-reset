SUMMARY = "aioredis Python Module"
LICENSE = "BSD"
DESCRIPTION = "asyncio (PEP 3156) Redis support"
LIC_FILES_CHKSUM = "file://LICENSE;md5=6d013dde180de30f9decd38fa3027de1"
PYPI_PACKAGE = "aioredis"
inherit setuptools3 pypi
SRC_URI[md5sum] = "fd6c66e4d8efce553c369c0855c6fb49"
SRC_URI[sha256sum] = "84d62be729beb87118cf126c20b0e3f52d7a42bb7373dc5bcdd874f26f1f251a"

RDEPENDS_${PN} += " \
    python3-async-timeout \
"
