SUMMARY = "whitenoise Python Module"
LICENSE = "MIT"
DESCRIPTION = "Radically simplified static file serving for WSGI applications"
LIC_FILES_CHKSUM = "file://LICENSE;md5=aba4901cc64e401cea5a267eac2a2e1e"
PYPI_PACKAGE = "whitenoise"
inherit setuptools3 pypi
SRC_URI[md5sum] = "d3d80d5c923a4c42fa404350dd27f82d"
SRC_URI[sha256sum] = "42133ddd5229eeb6a0c9899496bdbe56c292394bf8666da77deeb27454c0456a"
