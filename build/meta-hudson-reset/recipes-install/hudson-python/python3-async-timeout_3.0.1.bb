SUMMARY = "async_timeout Python Module"
LICENSE = "Apache-2.0"
DESCRIPTION = "Timeout context manager for asyncio programs"
LIC_FILES_CHKSUM = "file://LICENSE;md5=e3fc50a88d0a364313df4b21ef20c29e"
PYPI_PACKAGE = "async-timeout"
inherit setuptools3 pypi
SRC_URI[md5sum] = "305c4fa529f2485c403d0dbe14390175"
SRC_URI[sha256sum] = "0c3c816a028d47f659d6ff5c745cb2acf1f966da1fe5c19c77a70282b25f4c5f"
