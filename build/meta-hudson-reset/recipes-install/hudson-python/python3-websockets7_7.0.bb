SUMMARY = "Websockets Python Module"
LICENSE = "BSD"
DESCRIPTION = "Websockets is a library for building WebSocket servers and clients in Python with a focus on correctness and simplicity."
LIC_FILES_CHKSUM = "file://LICENSE;md5=5070256738c06d2e59adbec1f4057dac"
PYPI_PACKAGE = "websockets"
inherit setuptools3 pypi
SRC_URI[md5sum] = "e3b5f2e257de0371e7b4d0b6ef7bc29e"
SRC_URI[sha256sum] = "08e3c3e0535befa4f0c4443824496c03ecc25062debbcf895874f8a0b4c97c9f"
