SUMMARY = "miniupnpc Python Module"
LICENSE = "BSD"
DESCRIPTION = "MiniUPnP project"
LIC_FILES_CHKSUM = "file://LICENSE;md5=4a95d5317ee6dac993b7598848c72c1e"
PYPI_PACKAGE = "miniupnpc"
inherit setuptools3 pypi
SRC_URI[md5sum] = "e14eb34e0d7cf17e109648926578b733"
SRC_URI[sha256sum] = "7ea46c93486fe1bdb31f0e0c2d911d224fce70bf5ea120e4295d647dfe274931"
