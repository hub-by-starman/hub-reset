SUMMARY = "wsproto Python Module"
LICENSE = "MIT"
DESCRIPTION = "WebSockets state-machine based protocol implementation"
LIC_FILES_CHKSUM = "file://LICENSE;md5=69fabf732409f4ac61875827b258caaf"
PYPI_PACKAGE = "wsproto"
inherit setuptools3 pypi
SRC_URI[md5sum] = "1c7270a941cb20be0f8354f1c943b8ec"
SRC_URI[sha256sum] = "55c3da870460e8838b2fbe4d10f3accc0cea3a13d5e8dbbdc6da5d537d6d44dc"
