SUMMARY = "Uvicorn Module"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE.md;md5=206119e27d6b034e7ce70d73063c82a8"
DESCRIPTION = "The lightning-fast ASGI server."

SRC_URI[md5sum] = "43ee626744202556b88463c9d744f6cf"
SRC_URI[sha256sum] = "4d7db1e99a2749fb3b97e21bd4a70ff49621e6be03591a09d76c24fbcc30ea45"

PYPI_PACKAGE = "uvicorn"
inherit  setuptools3  pypi
FILES_${PN} += "/usr/share/LICENSE.md"
RDEPENDS_${PN} += " \
    ${PYTHON_PN}-asyncio \
    ${PYTHON_PN}-typing \
    ${PYTHON_PN}-misc \
    ${PYTHON_PN}-setuptools \
    python3-uvloop \
    python3-httptools \
    python3-websockets7 \
    python3-h11 \
    python3-wsproto \
    python3-click7 \
"
