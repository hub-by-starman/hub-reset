SUMMARY = "Channels Module"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://PKG-INFO;md5=d3b6add0b189c559388a1e70977ba08c"
inherit setuptools3 pypi
PYPI_PACKAGE = "channels"
SRC_URI = "${PYPI_SRC_URI} \
           file://0001-Removed-need-for-daphne.patch \
           file://0001-Removed-dependency-on-daphne.patch \
           "
SRC_URI[md5sum] = "4aceb805e3974680945d8fdcec02de95"
SRC_URI[sha256sum] = "e13ba874d854ac493ece329dcd9947e82357c15437ac1a90ed1040d0e5b87aad"

BBCLASSEXTEND = "native"
