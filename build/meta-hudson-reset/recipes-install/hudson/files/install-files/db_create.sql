CREATE USER hudson_db_user WITH PASSWORD 'hudson_db_password';
CREATE DATABASE hudson_db OWNER hudson_db_user;
SET client_encoding TO 'utf8';
SET default_transaction_isolation TO 'read committed';
SET timezone TO 'UTC';
