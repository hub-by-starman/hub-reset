#!/bin/bash
set -x

########CONFIGURE CONNECT DJANGO########
if [ ! -f /hudson/install/configured_connect_django ]; then
    (cd /hudson/site \
     && django-admin startproject hudson_connect \
     && cd /hudson/site/hudson_connect/hudson_connect \
     && django-admin startapp connector)
    #sed -i -e 's/DEBUG = True/DEBUG = False/g' /hudson/site/hudson_connect/hudson_connect/settings.py

    mv /hudson/tempsite/hudson_connect/routing.py /hudson/site/hudson_connect/hudson_connect/routing.py
    mv /hudson/tempsite/hudson_connect/asgi.py /hudson/site/hudson_connect/hudson_connect/asgi.py
    cat /hudson/tempsite/hudson_connect/settings.py >> /hudson/site/hudson_connect/hudson_connect/settings.py
    cat /hudson/tempsite/hudson_connect/connector/admin.py >> /hudson/site/hudson_connect/hudson_connect/connector/admin.py
    cat /hudson/tempsite/hudson_connect/connector/models.py >> /hudson/site/hudson_connect/hudson_connect/connector/models.py
    mv /hudson/tempsite/hudson_connect/connector/serializers.py /hudson/site/hudson_connect/hudson_connect/connector/serializers.py
    cat /hudson/tempsite/hudson_connect/connector/views.py >> /hudson/site/hudson_connect/hudson_connect/connector/views.py
    mv /hudson/tempsite/hudson_connect/connector/urls.py /hudson/site/hudson_connect/hudson_connect/connector/urls.py
    mv /hudson/tempsite/hudson_connect/connector/routing.py /hudson/site/hudson_connect/hudson_connect/connector/routing.py
    mv /hudson/tempsite/hudson_connect/connector/consumers.py /hudson/site/hudson_connect/hudson_connect/connector/consumers.py
    mv /hudson/tempsite/hudson_connect/connector/wifi.py /hudson/site/hudson_connect/hudson_connect/connector/wifi.py

    mv /hudson/tempsite/hudson_connect/urls.py /hudson/site/hudson_connect/hudson_connect/urls.py


    perl -i -0pe "s{\QALLOWED_HOSTS = []}{ALLOWED_HOSTS = ['192.168.4.1']}g" /hudson/site/hudson_connect/hudson_connect/settings.py
    perl -i -0pe "s{ntrib.staticfiles',}{ntrib.staticfiles',\n    'rest_framework.authtoken',\n    'channels',\n    'rest_framework',\n    'hudson_connect',\n    'hudson_connect.connector',}g" /hudson/site/hudson_connect/hudson_connect/settings.py
    perl -i -0pe "s{SecurityMiddleware',}{SecurityMiddleware',\n    'whitenoise.middleware.WhiteNoiseMiddleware',}g" /hudson/site/hudson_connect/hudson_connect/settings.py


    python3 /hudson/site/hudson_connect/manage.py makemigrations
    python3 /hudson/site/hudson_connect/manage.py migrate
    python3 /hudson/site/hudson_connect/manage.py collectstatic


    #Make the media directory
    mkdir /hudson/site/hudson_connect/media
    touch /hudson/install/configured_connect_django
fi
########DONE CONFIGURE CONNECT DJANGO########




########CONFIGURE DJANGO########
if [ ! -f /hudson/install/configured_main_django ]; then
    (cd /hudson/site \
     && django-admin startproject hudson_main \
     && cd /hudson/site/hudson_main/hudson_main \
     && django-admin startapp connection_check \
     && django-admin startapp sensorWorker \
     && django-admin startapp users)

    #sed -i -e 's/DEBUG = True/DEBUG = False/g' /hudson/site/hudson_main/hudson_main/settings.py
    mv /hudson/tempsite/hudson_main/routing.py /hudson/site/hudson_main/hudson_main/routing.py
    mv /hudson/tempsite/hudson_main/asgi.py /hudson/site/hudson_main/hudson_main/asgi.py
    cat /hudson/tempsite/hudson_main/settings.py >> /hudson/site/hudson_main/hudson_main/settings.py
    cat /hudson/tempsite/hudson_main/connection_check/admin.py >> /hudson/site/hudson_main/hudson_main/connection_check/admin.py
    cat /hudson/tempsite/hudson_main/connection_check/models.py >> /hudson/site/hudson_main/hudson_main/connection_check/models.py
    mv /hudson/tempsite/hudson_main/connection_check/serializers.py /hudson/site/hudson_main/hudson_main/connection_check/serializers.py
    mv /hudson/tempsite/hudson_main/connection_check/views.py /hudson/site/hudson_main/hudson_main/connection_check/views.py
    mv /hudson/tempsite/hudson_main/urls.py /hudson/site/hudson_main/hudson_main/urls.py
    mv /hudson/tempsite/hudson_main/connection_check/urls.py /hudson/site/hudson_main/hudson_main/connection_check/urls.py

    #Configure sensorWorker
    mkdir /hudson/site/hudson_main/hudson_main/sensorWorker/management
    mkdir /hudson/site/hudson_main/hudson_main/sensorWorker/management/commands
    cat /hudson/tempsite/hudson_main/sensorWorker/models.py >> /hudson/site/hudson_main/hudson_main/sensorWorker/models.py
    cat /hudson/tempsite/hudson_main/sensorWorker/management/commands/readTempSensor.py >> /hudson/site/hudson_main/hudson_main/sensorWorker/management/commands/readTempSensor.py
    mv /hudson/tempsite/hudson_main/sensorWorker/serializers.py /hudson/site/hudson_main/hudson_main/sensorWorker/serializers.py
    mv /hudson/tempsite/hudson_main/sensorWorker/urls.py /hudson/site/hudson_main/hudson_main/sensorWorker/urls.py
    mv /hudson/tempsite/hudson_main/sensorWorker/views.py /hudson/site/hudson_main/hudson_main/sensorWorker/views.py
    cat /hudson/tempsite/hudson_main/sensorWorker/admin.py >> /hudson/site/hudson_main/hudson_main/sensorWorker/admin.py
    #Done Configure sensorWorker

    #Configure Users
    mv /hudson/tempsite/hudson_main/users/serializers.py /hudson/site/hudson_main/hudson_main/users/serializers.py
    mv /hudson/tempsite/hudson_main/users/urls.py /hudson/site/hudson_main/hudson_main/users/urls.py
    mv /hudson/tempsite/hudson_main/users/views.py /hudson/site/hudson_main/hudson_main/users/views.py
    #Done Configure Users


    #perl -i -0pe "s{\QALLOWED_HOSTS = []}{ALLOWED_HOSTS = ['$local_ip', '$external_ip']}g" /hudson/site/hudson_main/hudson_main/settings.py
    perl -i -0pe "s{ntrib.staticfiles',}{ntrib.staticfiles','channels','rest_framework','hudson_main','hudson_main.connection_check','hudson_main.sensorWorker','rest_framework.authtoken'}g" /hudson/site/hudson_main/hudson_main/settings.py
    perl -i -0pe "s{SecurityMiddleware',}{SecurityMiddleware',\n    'whitenoise.middleware.WhiteNoiseMiddleware',}g" /hudson/site/hudson_main/hudson_main/settings.py
    echo 'AUTH_PASSWORD_VALIDATORS = []' >> /hudson/site/hudson_main/hudson_main/settings.py

    python3 /hudson/site/hudson_main/manage.py makemigrations
    python3 /hudson/site/hudson_main/manage.py migrate
    python3 /hudson/site/hudson_main/manage.py collectstatic
    touch /hudson/install/configured_main_django
fi

if [ ! -f /hudson/install/configured_django_variables ]; then
    # Add a hudson_main identification code
    brainIdentifierCode=$(head /dev/urandom | tr -dc A-Za-z0-9 | cut -c1-32)
    echo "from hudson_main.connection_check.models import hudsonIdentifier; hudsonIdentifier(hudsonIdentifierCode='$brainIdentifierCode').save()" | python3 /hudson/site/hudson_main/manage.py shell
    echo "from hudson_connect.connector.models import HudsonID; HudsonID(hudsonID='$brainIdentifierCode').save()" | python3 /hudson/site/hudson_connect/manage.py shell

    # Add a Port values to the database
    echo "from hudson_connect.connector.models import Port; Port(port=port_replace).save()" | python3 /hudson/site/hudson_connect/manage.py shell
    touch /hudson/install/configured_main_django
fi
########DONE CONFIGURE DJANGO########
