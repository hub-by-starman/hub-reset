#!/bin/bash

########INSTALL POSTGRESQL########
runuser -l  postgres -c "psql -c \"CREATE USER hudson_db_user WITH PASSWORD 'hudson_db_password';\""
sleep 5
runuser -l  postgres -c "createdb hudson_db --owner hudson_db_user"
systemctl enable postgresql
systemctl start postgresql

systemctl enable redis-server
systemctl start redis-server
########DONE INSTALL POSTGRESQL########
