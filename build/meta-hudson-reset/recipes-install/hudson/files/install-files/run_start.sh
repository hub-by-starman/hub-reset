#!/bin/bash

today=`date +%Y-%m-%d.%H:%M:%S`

# send stdout and stderr from rc.local to a log file
#exec 1>/hudson/install/install-files/$today-rc.local.log 2>&1
exec &> >(tee -a "/hudson/install/$today-rc.local.log")
# tell sh to display commands before execution
set -x

echo "Started rc.local"
if [ ! -f /hudson/install/has_booted ]; then
    echo "This is the first boot"

    ########GENERATE VARIABLES########
    if [ ! -f /hudson/install/variables_generated ]; then
        port=$(shuf -i 17923-64000 -n 1)
        perl -i -0pe "s{port_replace}{$port}g" /hudson/install/install-files/configure_django.sh
        perl -i -0pe "s{port_replace}{$port}g" /etc/nginx/hudson_main_nginx.conf
        touch /hudson/install/variables_generated
    fi
    ########DONE GENERATE VARIABLES########


    ########RUN SCRIPTS########
    if [ ! -f /hudson/install/configured_django ]; then
        echo "Configuring Django"
        sh /hudson/install/install-files/configure_django.sh
        touch /hudson/install/configured_django
    fi

    if [ ! -f /hudson/install/configured_sensor_capability ]; then
        echo "Setting Up Sensors"
        sh /hudson/install/install-files/setup_sensor_capability.sh
        systemctl restart sensorworker
        touch /hudson/install/configured_sensor_capability
    fi

    echo "Starting Uvicorn Servers"
    systemctl restart mainuvicorn
    systemctl restart connectuvicorn

    systemctl restart hudson_main_nginx
    sleep 5
    systemctl restart hudson_connect_nginx

    echo "Creating AP Network"
    systemctl enable wpa_supplicant@wlan0
    systemctl enable wpa_supplicant@wlan1
    systemctl start wpa_supplicant@wlan0
    systemctl start wpa_supplicant@wlan1


    ########DONE RUN SCRIPTS########



    touch /hudson/install/has_booted
    #reboot
fi

########RUN ON EVERY BOOT########
#systemctl daemon-reload
#systemctl restart wpa_supplicant@wlan0
########DONE RUN ON EVERY BOOT########

exit 0
