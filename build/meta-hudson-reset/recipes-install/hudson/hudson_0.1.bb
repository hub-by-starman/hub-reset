SUMMARY = "Hudson"
DESCRIPTION = "The core recipe to install all parts of hudson-reset"
LICENSE = "AGPL-3.0"
LIC_FILES_CHKSUM = "file://COPYING;md5=bf4f3c60e4d121211608f0ad0dbaf67c"
SRC_URI += "file://COPYING"
S = "${WORKDIR}"
REQUIRED_DISTRO_FEATURES= "systemd"
SRC_URI += "file://install-files"
SRC_URI += "file://services"
SRC_URI += "file://ca"

do_install() {
    install -d ${D}/hudson/site
    install -d ${D}/hudson/install/install-files
    install -d ${D}/hudson/install/services
    install -d ${D}/hudson/ca

    install -Dm 0700 ${WORKDIR}/install-files/* ${D}/hudson/install/install-files/
    install ${WORKDIR}/services/* ${D}/hudson/install/services/

    # Install Services
    install -d ${D}${systemd_system_unitdir}
    cp ${WORKDIR}/services/* ${D}${systemd_system_unitdir}

    # Setup nginx
    install -d ${D}/etc/nginx
    cp ${WORKDIR}/install-files/hudson_main_nginx.conf ${D}/etc/nginx
    cp ${WORKDIR}/install-files/hudson_connect_nginx.conf ${D}/etc/nginx

    install ${WORKDIR}/ca/* ${D}/hudson/ca/
}
inherit systemd
FILES_${PN} += "/hudson"

FILES_${PN} += "${systemd_system_unitdir}"

SYSTEMD_SERVICE_${PN} = "\
    installHudson.service \
    mainuvicorn.service \
    connectuvicorn.service \
    hudson_main_nginx.service \
    hudson_connect_nginx.service \
    sensorWorker.service \
"

RDEPENDS_${PN} += " \
    hudson-connect \
    hudson-main \
    hudson-ap-setup \
    nginx \
    openssl-bin \
    curl \
    bash \
    perl \
    redis \
"

# Install python modules
RDEPENDS_${PN} += " \
    python3-channels \
    python3-djangorestframework \
    python3-uvicorn \
    python3-sqlite3 \
    python3-asgiref \
    python3-whitenoise \
    python3-channels-redis \
    python3-aioredis \
"
BBCLASSEXTEND = "native nativesdk"
