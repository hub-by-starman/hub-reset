SUMMARY = "Hudson AP Setup"
DESCRIPTION = "Setup the AP networking required for connections between Brain and device"
LICENSE = "AGPL-3.0"
LIC_FILES_CHKSUM = "file://COPYING;md5=bf4f3c60e4d121211608f0ad0dbaf67c"
SRC_URI += "file://ap-files"
SRC_URI += "file://COPYING"
do_install() {
    install -d ${D}/hudson/install/ap-files
    install -Dm 0700 ${WORKDIR}/ap-files/* ${D}/hudson/install/ap-files

    install -d ${D}/etc/wpa_supplicant
    cp -r ${WORKDIR}/ap-files/wpa_supplicant-wlan0.conf ${D}/etc/wpa_supplicant
    cp -r ${WORKDIR}/ap-files/wpa_supplicant-wlan1.conf ${D}/etc/wpa_supplicant

    install -d ${D}/etc/systemd/network
    cp -r ${WORKDIR}/ap-files/08-wlan0.network ${D}/etc/systemd/network
    cp -r ${WORKDIR}/ap-files/12-wlan1.network ${D}/etc/systemd/network
}
FILES_${PN} += "/hudson/install"
S = "${WORKDIR}"

RDEPENDS_${PN} += " \
    linux-firmware \
    bash \
    haveged \
"
