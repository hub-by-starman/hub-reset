SUMMARY = "Hudson Connect Site"
DESCRIPTION = "The Hudson connect django servers  allows the user to interface with it in order to pair a device with a Brain"
LICENSE = "AGPL-3.0"
LIC_FILES_CHKSUM = "file://COPYING;md5=bf4f3c60e4d121211608f0ad0dbaf67c"

SRC_URI += "file://hudson_connect"
SRC_URI += "file://COPYING"
do_install() {
    install -d ${D}/hudson/tempsite
    cp -r ${WORKDIR}/hudson_connect ${D}/hudson/tempsite
}
FILES_${PN} += "/hudson/tempsite"
S = "${WORKDIR}"
RDEPENDS_${PN} += " \
    python3-systemd \
    python3-miniupnpc \
    python3-whitenoise \
"
