from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
import hudson_connect.connector.routing

application = ProtocolTypeRouter({
    'websocket': AuthMiddlewareStack(
        URLRouter(
            hudson_connect.connector.routing.websocket_urlpatterns
        )
    ),
})
