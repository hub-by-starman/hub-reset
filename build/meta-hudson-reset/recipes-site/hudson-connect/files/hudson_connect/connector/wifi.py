"""
Functions related to modifying system wifi settings in python
"""
import os
import shutil
import subprocess
import select
import time
import socket
import shlex
import urllib.request
from systemd import journal
import miniupnpc
from .models import GeneratedCerts, LocalIP, ExternalIP, SSID, Port

def watchJournal(entry, ssid):
    """Handles messages recieved by the journal and processes Wifi failures/successes"""


    if "wpa_supplicant@wlan0.service: Failed with result 'exit-code'." in entry['MESSAGE']:
        print("Recieved a password too short error.")
        replaceWifiStatus = {}
        replaceWifiStatus['portForwarded'] = False
        replaceWifiStatus['wifiConnected'] = False
        replaceWifiStatus['error'] = "Password too short"
        return replaceWifiStatus

    if "wlan0: WPA: 4-Way Handshake failed - pre-shared key may be incorrect" in entry['MESSAGE']:
        print("Recieved an incorrect password error")
        replaceWifiStatus = {}
        replaceWifiStatus['portForwarded'] = False
        replaceWifiStatus['wifiConnected'] = False
        replaceWifiStatus['error'] = "Password was incorrect"
        return replaceWifiStatus

    if "wlan0: CTRL-EVENT-CONNECTED" in entry['MESSAGE']:
        print("Successful connection")
        SSID(ssid=ssid).save()
        return portForwardStatus(ssid)
    return None


def replaceWifi(ssid, psk, countryCode):
    """Change wifi network in the system to a different provided network"""

    #Handle a blank password (Later we will make sure to test more than just WPA2)
    if psk is None:
        psk = ""


    #Remove any existing wifi config files and replace with the default
    os.remove("/hudson/install/ap-files/wpa_supplicant-wlan0.conf")
    shutil.copy2('/hudson/install/ap-files/wpa_supplicant-wlan0-temp.conf',
                 '/etc/wpa_supplicant/wpa_supplicant-wlan0.conf')
    #Replace the data in the new conf file
    with open("/etc/wpa_supplicant/wpa_supplicant-wlan0.conf") as wpaSupplicantFile:
        newText = wpaSupplicantFile.read().replace('SSID_REPLACE', ssid).replace(
            'PSK_REPLACE', psk).replace('COUNTRY_REPLACE', countryCode)

    with open("/etc/wpa_supplicant/wpa_supplicant-wlan0.conf", "w") as wpaSupplicantFile:
        wpaSupplicantFile.write(newText)


    print("Attempting to reset the wifi in bash")
    subprocess.call("/hudson/install/ap-files/replace_wifi.sh", shell=True)
    print("Done resetting the wifi in bash")

    systemJournal = journal.Reader()
    systemJournal.log_level(journal.LOG_INFO)
    systemJournal.seek_tail()
    systemJournal.get_previous()
    poll = select.poll()
    poll.register(systemJournal, systemJournal.get_events())
    twentyFiveSeconds = time.time() + 25

    while time.time() < twentyFiveSeconds:
        if poll.poll(250):
            if systemJournal.process() == journal.APPEND:
                for entry in systemJournal:
                    journalValue = watchJournal(entry, ssid)
                    if journalValue is not None:
                        return journalValue

    replaceWifiStatus = {}
    replaceWifiStatus['portForwarded'] = False
    replaceWifiStatus['wifiConnected'] = False
    replaceWifiStatus['error'] = "Failed to Connect: Timed Out"
    return replaceWifiStatus


def checkHost(port):
    """Check if a port is open and return true or false"""
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = sock.connect_ex(('127.0.0.1', port))
    if result == 0:
        print("Port is open")
        return True

    print("Port is not open")
    return False

def portForwardStatus(ssid):
    """Check if a port has been forwarded and attempt to forward it. Return a status of failure."""

    port = Port.objects.values_list('port', flat=True)[0]
    time.sleep(3)

    # We don't need the External IP, but this does generate the required certs
    localIP = getLocalIP()
    externalIP = getExternalIP()

    upnp = miniupnpc.UPnP()
    upnp.discoverdelay = 10
    upnp.discover()
    try:
        upnp.selectigd()
        upnp.addportmapping(port, 'TCP', upnp.lanaddr, port, 'testing', '')
        if checkHost(port):
            replaceWifiStatus = {}
            replaceWifiStatus['portForwarded'] = True
            replaceWifiStatus['wifiConnected'] = True
            replaceWifiStatus['externalIP'] = externalIP
            replaceWifiStatus['localIP'] = localIP
            return replaceWifiStatus

        replaceWifiStatus = {}
        replaceWifiStatus['portForwarded'] = False
        replaceWifiStatus['wifiConnected'] = True
        replaceWifiStatus['error'] = "UPNP router found but failed to set UPNP anyway"
        replaceWifiStatus['port'] = port
        replaceWifiStatus['gatewayIP'] = getGatewayIP()
        replaceWifiStatus['ssid'] = ssid
        replaceWifiStatus['externalIP'] = externalIP
        replaceWifiStatus['localIP'] = localIP
        return replaceWifiStatus

    except Exception as exception:
        if str(exception.args) == "('No UPnP device discovered',)":
            replaceWifiStatus = {}
            replaceWifiStatus['portForwarded'] = False
            replaceWifiStatus['wifiConnected'] = True
            replaceWifiStatus['error'] = "No upnp devices found on network"
            replaceWifiStatus['port'] = port
            replaceWifiStatus['gatewayIP'] = getGatewayIP()
            replaceWifiStatus['ssid'] = ssid
            replaceWifiStatus['externalIP'] = externalIP
            replaceWifiStatus['localIP'] = localIP
            return replaceWifiStatus
        raise

def getNearbyNetworks():
    """
        Scan for nearby wifi networks and return a list of dicts which contain the encryption type,
        whether there is encryption, and the ssid of the network
    """
    lines = runBashScan()
    networks = []
    network = {}
    for line in lines:
        if "SSID: " in line:

            # Check if a network object exists
            if 'ssid' in network:

                duplicate = False
                brainNetwork = False

                for filteredNetwork in networks:
                    # Check for duplicates
                    if filteredNetwork['ssid'] == network['ssid']:
                        duplicate = True
                        break

                # Don't add Hudson networks
                if "Hudson-" in network['ssid']:
                    brainNetwork = True

                if not duplicate and not brainNetwork:
                    if 'encryptionType' in network:
                        network['encrypted'] = True
                    else:
                        network['encrypted'] = False
                        network['encryptionType'] = "Open"
                    networks.append(network)
                    network = {}
            # Get SSID and replace \x20 with spaces
            ssid = line.split("SSID: ", 1)[1].replace(r"\x20", " ")
            network['ssid'] = ssid

        if "WPA:" in line:
            network['encryptionType'] = "WPA"
        if "WEP:" in line:
            network['encryptionType'] = "WEP"
    return networks

def runBashScan():
    """Use bash to scan for networks and return lines of network data"""

    # Sometimes the command to get wifi networks fails the first time.
    # Try multiple times.
    for retries in range(0, 4):
        print("Scanning for networks")
        try:
            proc = subprocess.check_output(shlex.split("/bin/bash /hudson/install/ap-files/iw.sh"),
                                           stderr=subprocess.STDOUT)
            lines = proc.decode('utf-8').splitlines()
            print("Network Scan Result: " + lines[0])
            return lines

        except subprocess.CalledProcessError as error:
            if "command failed: Device or resource busy" in str(error.output):
                print("Error scanning, retrying " + str(retries + 1))
                time.sleep(1)

def getGatewayIP():
    """Get the current gateway IP or retrun no connectivity"""

    gatewayIPValue = "No wifi connectivity"
    strs = subprocess.check_output(shlex.split('ip r l'))
    for line in strs.decode("UTF-8").splitlines():
        if "wlan0" in line:
            gatewayIPValue = line.split()[0]
    return gatewayIPValue

def getLocalIP():
    """Get the current local IP or retrun no connectivity"""

    localIP = "No wifi connectivity"
    lines = subprocess.check_output(shlex.split('ip r l'))
    for line in lines.decode("UTF-8").splitlines():
        if "wlan0" in line:
            localIP = line.split()[6]
            LocalIP(localIP=localIP).save()
            # Update the IP in django settings
            externalIPList = ExternalIP.objects.values_list('externalIP', flat=True).order_by('-id')
            if externalIPList.count() == 0:
                getExternalIP()
    return localIP

def getExternalIP():
    """Get the current external IP or retrun no connectivity"""

    for attempt in range(0, 3):
        try:
            externalIP = urllib.request.urlopen('https://ident.me').read().decode('utf8')
            print("Found external connectivity at: " + externalIP)

            ExternalIP(externalIP=externalIP).save()
            # Update the IP in django settings
            localIPList = LocalIP.objects.values_list('localIP', flat=True).order_by('-id')
            if localIPList.count() > 0:
                localIP = localIPList[0]
            else:
                localIP = getLocalIP()

            replaceSettingsAllowedHost(externalIP, localIP)

            # Generate TLS certs for new external IP
            generateCerts(externalIP)
            break
        except urllib.error.URLError:
            print("No external connectivity found on try: " + str(attempt+1))
            externalIP = "No External Connectivity"
            time.sleep(5)

    return externalIP

def getSSID():
    """Get the current SSID using iwconfig"""

    lines = subprocess.check_output(shlex.split('iwconfig'))
    for line in lines.decode("UTF-8").splitlines():
        if "wlan0" in line:
            ssidValue = line.split()[3]
            ssidValue = ssidValue[7:][:-1]
    return ssidValue

def replaceSettingsAllowedHost(externalIP, localIP):
    """Replace the allowed hosts for the main Hudson server"""

    with open('/hudson/site/hudson_main/hudson_main/settings.py', 'r') as file:
        data = file.readlines()
    data[27] = ("ALLOWED_HOSTS = ['%s', '%s']\n" % (externalIP, localIP))
    with open('/hudson/site/hudson_main/hudson_main/settings.py', 'w') as file:
        file.writelines(data)
    subprocess.check_call(['systemctl', 'restart', 'mainuvicorn'])

def generateCerts(externalIP):
    """Generate mutual TLS certs using the setup_mutual_tls.sh file"""

    # Check if certs have already been generated for this IP address
    print("Checking certs")
    generatedCertsList = GeneratedCerts.objects.values_list('ip', flat=True)
    if generatedCertsList.count() > 0 and externalIP in generatedCertsList:
        # Cert already generated for this address. Do nothing
        print("Cert already generated for IP " + externalIP)
        return

    print("Cert not generated for IP " + externalIP)
    subprocess.call("/hudson/install/install-files/setup_mutual_tls.sh", shell=True)
    # Save this IP so we know the certs for this IP have been generated
    GeneratedCerts(ip=externalIP).save()
