class HudsonID(models.Model):
    hudsonID = models.CharField(max_length=32)

class ExternalIP(models.Model):
    externalIP = models.CharField(max_length=16)

class LocalIP(models.Model):
    localIP = models.CharField(max_length=16)

class Port(models.Model):
    port = models.PositiveIntegerField()

class SSID(models.Model):
    ssid = models.CharField(max_length=256)

class IPData(models.Model):
    externalIP = models.ForeignKey(ExternalIP, on_delete=models.PROTECT)
    localIP = models.ForeignKey(LocalIP, on_delete=models.PROTECT)
    port = models.ForeignKey(Port, on_delete=models.PROTECT)
    ssid = models.ForeignKey(SSID, on_delete=models.PROTECT)
    hudsonID = models.ForeignKey(HudsonID, on_delete=models.PROTECT)

class GeneratedCerts(models.Model):
    ip = models.CharField(max_length=16)
