from rest_framework import serializers
from .models import IPData
#from .models import ip_data, hasWifi


class IPDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = IPData
        fields = (
            'externalIP',
            'localIP',
            'ssid',
            'port',
            'hudsonID'
        )

# class HasWifiSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = hasWifi
#         fields = (
#             'hasWifi',
#         )
