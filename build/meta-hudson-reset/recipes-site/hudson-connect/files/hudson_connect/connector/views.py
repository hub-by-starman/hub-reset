from django.shortcuts import render
from rest_framework import viewsets
from .models import IPData
#from .models import ip_data, hasWifi
from .serializers import IPDataSerializer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status


class IPDataView(APIView):
    def get(self, request, format=None):
        queryset = IPData.objects.all()
        serializer = IPDataSerializer(queryset, many=True)
        return Response(serializer.data)

# class HasWifiView(APIView):
#     def get(self, request, format=None):
#         queryset = hasWifi.objects.all()
#         serializer = HasWifiSerializer(queryset, many=True)
#         return Response(serializer.data)
