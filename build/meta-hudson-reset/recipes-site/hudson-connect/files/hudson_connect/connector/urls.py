from django.conf.urls import url
from hudson_connect.connector.views import IPDataView
#from hudson_connect.connector.views import IPDataView, HasWifiView

urlpatterns = [
    url(r'^ip_data/',  IPDataView.as_view(), name='ip_data')
    #url(r'^hasWifi/',  HasWifiView.as_view(), name='hasWifi')
]
