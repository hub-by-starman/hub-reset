from channels.generic.websocket import AsyncWebsocketConsumer
import json
from .wifi import replaceWifi, getNearbyNetworks, getLocalIP, getExternalIP
from .models import HudsonID, Port, SSID

class mainConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        await self.accept()

    async def disconnect(self, close_code):
        print("Websocket Disconnected" + str(close_code))
        pass

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)

        if 'checkWifi' in text_data_json:
            print ("Recieved a request check if Hudson has wifi")

            wifiStatusObject = {}
            localIP = getLocalIP()
            if localIP == "No wifi connectivity":
                wifiStatusObject['localConnected'] = False
                wifiStatusObject['externalConnected'] = False
            else:
                wifiStatusObject['localConnected'] = True
                wifiStatusObject['localIP'] = localIP
                externalIP = getExternalIP()
                if externalIP == "No External Connectivity":
                    wifiStatusObject['externalConnected'] = False
                else:
                    wifiStatusObject['externalConnected'] = True
                    wifiStatusObject['externalIP'] = externalIP


            wifiStatusObjectContainer = {}
            wifiStatusObjectContainer['wifiConnectionCheck'] = wifiStatusObject
            wifiStatusObjectContainer = json.dumps(wifiStatusObjectContainer)
            await self.send(wifiStatusObjectContainer)

        if 'lookingForNetworks' in text_data_json:
            print ("Recieved a request to send Wifi Network data")
            nearbyNetworks = getNearbyNetworks()
            nearbyNetworksName = {}
            nearbyNetworksName['nearByNetworks'] = nearbyNetworks
            nearbyNetworksJSON = json.dumps(nearbyNetworksName)
            await self.send(nearbyNetworksJSON)

        if 'wifiDetails' in text_data_json:
            wifiDetails = text_data_json['wifiDetails']
            print ("Received wifi info for input")

            wifiReplaceStatusName = {}
            wifiReplaceStatusName['wifiReplaceDone'] = False
            wifiReplaceStatusJSON = json.dumps(wifiReplaceStatusName)
            await self.send(wifiReplaceStatusJSON)

            print ("Begining Wifi Replacing")
            wifiReplaceStatus = replaceWifi(wifiDetails['ssid'],
                wifiDetails['psk'], wifiDetails['countryCode'])

            wifiReplaceStatusName = {}
            wifiReplaceStatusName['wifiReplaceStatus'] = wifiReplaceStatus
            wifiReplaceStatusJSON = json.dumps(wifiReplaceStatusName)
            await self.send(wifiReplaceStatusJSON)

        if 'checkPortForward' in text_data_json:
            portForwardDetails = text_data_json['wifiDetails']
            print ("Received a request for port forward info")
            wifiReplaceStatus = replaceWifi(wifiDetails['ssid'],
                wifiDetails['psk'], wifiDetails['countryCode'])

            wifiReplaceStatusName = {}
            wifiReplaceStatusName['wifiReplaceStatus'] = wifiReplaceStatus
            wifiReplaceStatusJSON = json.dumps(wifiReplaceStatusName)
            await self.send(wifiReplaceStatusJSON)

        if 'getIPData' in text_data_json:
            print ("Received request for IP Data")
            ssid = SSID.objects.values_list('ssid', flat=True)
            port = Port.objects.values_list(
                'port', flat=True)
            hudsonID = HudsonID.objects.values_list('hudsonID', flat=True)

            IPData = {}
            IPData['port'] = port[0]
            IPData['ssid'] = ssid[0]
            IPData['hudsonID'] = hudsonID[0]
            IpDataName = {}
            IpDataName['IPData'] = IPData
            IpDataJSON = json.dumps(IpDataName)
            await self.send(IpDataJSON)
