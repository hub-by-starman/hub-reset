ASGI_APPLICATION = "hudson_connect.routing.application"

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {
            "hosts": [("localhost", 6379)]
        }
    }
}
STATIC_ROOT = "/hudson/site/hudson_connect/static"
STATIC_URL = "/static/"
MEDIA_ROOT = "/hudson/site/hudson_connect/media"
MEDIA_URL = "/media/"
