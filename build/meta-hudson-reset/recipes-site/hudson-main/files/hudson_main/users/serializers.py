from rest_framework import serializers
from django.contrib.auth.models import User
from rest_framework.validators import UniqueValidator

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'username','email'
        )


class CreateUserSerializer(serializers.ModelSerializer):
    username = serializers.CharField(max_length = 16,validators=[UniqueValidator(queryset=User.objects.all())])
    password = serializers.CharField(max_length = 16, required = False)
    email = serializers.CharField()

    def create(self, validated_data):
        user = User.objects.create_user(validated_data['username'],validated_data['email'], validated_data['password'])
        return user

    class Meta:
        model = User
        fields = ('id','username','email','password')
