from django.conf.urls import url
from rest_framework import routers
from .views import UsersView, UserCreate, LoginCheck
router = routers.DefaultRouter()
from django.conf.urls import include
from rest_framework.authtoken import views

urlpatterns = router.urls
urlpatterns = [
    url(r'^users/',  UsersView.as_view(), name='users'),
    url(r'^create_user/', UserCreate.as_view(), name='account-create'),
    url(r'^loginCheck/', LoginCheck.as_view(), name='login-check'),
    # url(r'^auth/', include('rest_framework.urls')),
    url(r'^login/', views.obtain_auth_token)
]
