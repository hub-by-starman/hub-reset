from allauth.account.adapter import DefaultAccountAdapter
from rest_framework.validators import UniqueValidator
from allauth.account.utils import get_user_model

class MyAccountAdapter(DefaultAccountAdapter):

    def clean_password(self, password):
        if len(password) < 16:
            return password
        else:
            raise ValidationError("Error, password too long")

    def clean_username(self, username):
        if ( (len(username) > 16)):
            raise ValidationError("Error, username too long")
        elif ((len(username) == 0)):
            raise ValidationError("Error, username required")
        elif (username_exists(username)):
            raise ValidationError("Error, username exists")
        else:
            return username

    def new_user(self, request):
        """
        Instantiates a new User instance.
        """
        user = get_user_model()()
        return user
