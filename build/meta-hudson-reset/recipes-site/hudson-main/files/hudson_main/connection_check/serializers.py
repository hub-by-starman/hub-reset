from rest_framework import serializers
from .models import hudsonIdentifier
class HudsonIdentifierSerializer(serializers.ModelSerializer):
    class Meta:
        model = hudsonIdentifier
        fields = (
            'hudsonIdentifierCode',
        )
