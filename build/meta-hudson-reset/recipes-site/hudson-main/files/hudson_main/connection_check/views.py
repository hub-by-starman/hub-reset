from rest_framework import viewsets
from .models import hudsonIdentifier
from .serializers import HudsonIdentifierSerializer
from rest_framework.response import Response
from rest_framework.views import APIView

class HudsonIdentifierView(APIView):
    def get(self, request, format=None):
        queryset = hudsonIdentifier.objects.all()
        serializer = HudsonIdentifierSerializer(queryset, many=True)
        return Response(serializer.data)
