from django.conf.urls import url
from rest_framework import routers
from .views import HudsonIdentifierView
router = routers.DefaultRouter()
urlpatterns = router.urls
urlpatterns = [
    url(r'^hudson_identifier/',  HudsonIdentifierView.as_view(),
        name='hudsonIdentifier'),
]
