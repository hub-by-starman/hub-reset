#from channels import Group
from django.core.management import BaseCommand
import time
import os
import RPi.GPIO as GPIO
import glob
from hudson_main.sensorWorker.models import measured_temperature, sensors
from hudson_main.connection_check.models import hudsonIdentifier

import json
from datetime import datetime, timedelta
from threading import Thread

class Command(BaseCommand):


    def get_raw_temp(self, device_file):

        f = open(device_file, 'r')
        lines = f.readlines()
        f.close()
        return lines

    def get_temp_celcius(self, device_file):

    	lines = self.get_raw_temp(device_file)
    	while lines[0].strip()[-3:] != 'YES':
    		time.sleep(0.2)
    		lines = get_raw_temp()


    	temp_output = lines[1].find('t=')

    	if temp_output != -1:
    		temp_string = lines[1].strip()[temp_output+2:]
    		temp_c = float(temp_string) / 1000.0


    	return temp_c

    def sensorRun(self, device_file, deviceId):
        x = 0
        temperature1 = 0
        temperature2 = 0
        time = datetime.now() - timedelta(minutes=20)

        while True:


            #Compare current temp measurement with last and only save if different.
            temperature1 = self.get_temp_celcius(device_file)
            if (abs(temperature1 - temperature2) > 0.1):
                temperature2 = self.get_temp_celcius(device_file)

                #Group("users").send({'text': json.dumps({ 'measured_temp': temperature1})})
                now = datetime.now()
                difference = now - time
                if ( difference.total_seconds() > 300 ):

                    time = datetime.now()
                    save_temp = measured_temperature(measured_temp= temperature1, sensorID= deviceId)
                    save_temp.save()


    def handle(self, *args, **options):

        #Check for devices
        os.system('modprobe w1-gpio')
        os.system('modprobe w1-therm')
        base_dir = '/sys/bus/w1/devices/'
        device_folders = glob.glob(base_dir + '28*')

        hudsonID = hudsonIdentifier.objects.values_list(
            'hudsonIdentifierCode', flat=True)[0]
        print("Hudson ID: ")
        print(hudsonID),
        if (len(device_folders) > 0):
            print("Connected sensors found")
            for x in device_folders:

                device_folder = x
                device_file = device_folder + '/w1_slave'

                #Check if sensor is in the database
                deviceId = x[28:]
                print("Device ID: ")
                print(deviceId)
                databaseId = sensors.objects.filter(sensorID=deviceId)
                if (len(databaseId) == 0):
                    print("Device is not in database, adding...")
                    sensors(type="tempSensor", sensorID= deviceId, connectedDeviceID=hudsonID).save()

                t = Thread(target=self.sensorRun, args=(device_file,deviceId,))
                t.start()

        else:
            print("No sensors found")
