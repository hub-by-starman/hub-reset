class measured_temperature(models.Model):
    measured_temp = models.DecimalField(default=0, max_digits=5, decimal_places=2)
    measured_time = models.DateTimeField(editable=False, auto_now_add=True)
    sensorID = models.CharField(max_length=16)

class Average(models.Model):
    average = models.FloatField(default=0)

class Active_Times(models.Model):
    start_time = models.DateTimeField(editable=True, blank=True, null=True)
    finish_time = models.DateTimeField(editable=True, blank=True, null=True)

class relay_status(models.Model):
    status = models.CharField(max_length=12)

class weights(models.Model):
    weight = models.FloatField(default=0,)
    average = models.FloatField(default=0)

class sensors(models.Model):
    type = models.CharField(max_length=16)
    sensorID = models.CharField(max_length=7)
    connectedDeviceID = models.CharField(max_length=32)
