from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^hudson_identifier_api/', include('hudson_main.connection_check.urls')),
    url(r'^sensorworker_api/', include('hudson_main.sensorWorker.urls')),
    url(r'^users_api/', include('hudson_main.users.urls')),
]
