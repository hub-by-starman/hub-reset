SUMMARY = "Hudson Main Site"
DESCRIPTION = "The Hudson Main django servers  allows handles connections between devices"
LICENSE = "AGPL-3.0"
LIC_FILES_CHKSUM = "file://COPYING;md5=bf4f3c60e4d121211608f0ad0dbaf67c"

SRC_URI += "file://hudson_main"
SRC_URI += "file://COPYING"
do_install() {
    install -d ${D}/hudson/tempsite
    cp -r ${WORKDIR}/hudson_main ${D}/hudson/tempsite
}
FILES_${PN} += "/hudson/tempsite"
S = "${WORKDIR}"
