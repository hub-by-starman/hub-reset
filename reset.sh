#!/bin/bash
# We want p2p to work. The Brain(B) securely connects to the android app on user1's phone (U1).
# B sends U1 its cert(?). U1 stores it for later. When U1 later connects over an insecure connection (WAN), then it will verify using the cert
# B stores U1's IP and vice-versa.
# U1 can do the same form of nearby pairing with U2 using an AP network or bluetooth.
# If U1 changes its IP it can still connect to B. But B needs proof this is still U1. PKE will be used on connect.
# If B changes it's IP then it will send a message out to U1 to say it's IP has changed. Again PKE used.
# Need to know if there's a way to use E2E PKE as a replacement for TLS. This may also be possible with tls client auth (mutual auth)
# If both U1 and B change IP this will be a problem. B will ping all known IPs for U1 and on failing will notify U2.
# U1 will also try B and upon failing will update U2. If U2 has B's new IP then it will send it. Allowing U1 to connect to B.
# If no U2 exists in this situation a resync is required sadly.

# Despite being p2p the B is the controller. When U1 changes its IP it is the job of B to verify the new IP and send it to U2,U3...

function verifyGitFolder {
	if [ ! -d $1 ]; then
		sudo -u $SUDO_USER git clone -b thud $2
	else
		cd $1 || exit
		git checkout thud
		git pull
		cd ..
	fi
}


function downloadBuildFiles {
	cd build || { echo "Couldn't find folder 'build'" ; exit 1; }
	verifyGitFolder "poky" "git://git.yoctoproject.org/poky"

	cd poky || { echo "Couldn't find folder 'build/poky'" ; exit 1; }
	verifyGitFolder "poky/meta-cloud-services" "git://git.yoctoproject.org/meta-cloud-services"
	verifyGitFolder "poky/meta-openembedded" "git://git.openembedded.org/meta-openembedded"
	verifyGitFolder "poky/meta-raspberrypi" "git://git.yoctoproject.org/meta-raspberrypi"
	verifyGitFolder "poky/meta-raspberrypi" "git://git.yoctoproject.org/meta-raspberrypi"
	verifyGitFolder "poky/meta-virtualization" "git://git.yoctoproject.org/meta-virtualization"


	cd ..
	rm -rf poky/meta-hudson-reset
	# Move hudson files to build folder
	sudo -u $SUDO_USER cp -r meta-hudson-reset poky
	sudo -u $SUDO_USER mkdir -p poky/build/conf
	sudo -u $SUDO_USER cp bblayers.conf poky/build/conf
	sudo -u $SUDO_USER cp local.conf poky/build/conf
}

function buildHudson {
	printf "Starting build, this will take a while...\n"
	# DO A DOWNLOAD BUILD FILES FUNCTION
	downloadBuildFiles
	cd poky || { echo "Couldn't find folder 'build/poky'" ; exit 1; }
	source oe-init-build-env
	sudo -u $SUDO_USER bitbake core-image-base || exit 0
	if [ ! -f tmp/deploy/images/raspberrypi3/core-image-base-raspberrypi3.rpi-sdimg ]; then
    	echo "File not found error. The build failed. Please consult the log"
		exit 1
	fi
	printf "Build finally done\n"

	# Move image to deploy folder with date
	tempHudsonImage="$(find tmp/deploy/images/raspberrypi3 -maxdepth 1 -name "core-image-base-raspberrypi3.rpi-sdimg")"
	rm ../../deploy/*
	currentDate=$(date +%Y-%m-%d)
	cp $tempHudsonImage ../../deploy/$currentDate-Hudson.img
	hudsonImage=../../deploy/$currentDate-Hudson.img
}

if [ "$(id -u)" != "0" ]; then
	echo "Please run as root" 1>&2
	exit 1
fi

UseExistingImage="0"

#CHECK IF OS IMAGE EXISTS
mkdir -p build/deploy
hudsonImage=(`find build/deploy -maxdepth 1 -name "*Hudson.img"`)
if [ ${#hudsonImage[@]} -gt 0 ]; then

	#Get the date of the previous image to display
	filename=$hudsonImage
	num=${filename#*/}
	tmp=${num#*/}
	date=${tmp%-H*}

	printf "An image of the Hudson OS already exists.\n"
	printf "This image is from $date\n"

	while [[ "$UseExistingImage" != "1" && "$UseExistingImage" != "2" ]]
	do
		read -p "Enter 1 to use this image. Enter 2 to download or build a new image: " UseExistingImage
		if [[ "$UseExistingImage" != "1" && "$UseExistingImage" != "2" ]]; then
			printf "You must enter either 1 or 2\n"
		fi
	done
fi

if [ "$UseExistingImage" = "1" ]; then
	#JUST FLASH THE IMAGE
	printf "Flashing the image\n"
else
	DownloadOrBuild="0"
	while [[ "$DownloadOrBuild" != "1" && "$DownloadOrBuild" != "2" ]]
	do
	    read -p "Enter 1 to download the OS (fast) or enter 2 to build it yourself (slow): " DownloadOrBuild
		if [[ "$DownloadOrBuild" != "1" && "$DownloadOrBuild" != "2" ]]; then
			printf "You must enter either 1 or 2\n"
		fi
	done

	if [ "$DownloadOrBuild" = "1" ]; then
		#DOWNLOAD THE RELEASE IMAGE (DOESN'T EXIST YET)
		printf "Downloading Hudson...\n"
	else
		#BUILD THE BITCH
		buildHudson
	fi
fi

#We should now have an image ready to manipulate

selected_device="none"

function check_device {
	IFS=$'\n'
	DEVICES=( $(lsblk -d -o MODEL,PATH | tail -n +2) )
	lastDevicePosition=$(( ${#DEVICES[*]} - 1 ))
	lastDevice=${DEVICES[$lastDevicePosition]}
	for device in "${DEVICES[@]}"
	  do
		IFS=$'\t'
		deviceModel=$(echo "$device" | awk '{print $1}')
		devicePath=$(echo "$device" | awk '{print $2}')

		#TEST FOR ANY EXTERNAL SD CARD READERS
		if [[ $deviceModel = *"Multi-Card"* ]] | [[ $deviceModel = *"SD_MMC"* ]]; then
		    read -p "Is your SD card $devicePath?(y/n)  " device_check_response
		    printf "\n"
		    if [[ $device_check_response = "y" ]]; then
		        selected_device="$devicePath"
		        break
		    fi
		fi


		if [[ $device == "$lastDevice" ]]
		then
		    printf "No devices found, enter a device and press enter\n"
		    read
		fi


	done
}


while [[ $selected_device = "none" ]]
do
    check_device
done

printf "Selected device is: ${selected_device}\n"
IFS=$'\n'
MOUNTED_PARTITIONS=$(mount | grep -oh "\w*$selected_device\w*")
printf "Wiping Brain."

for word in $MOUNTED_PARTITIONS
do
    printf "."
    umount $word
    mkdosfs -F 32 -I $word >/dev/null
done

printf "\nInstalling New Brain...\n"
dd bs=4M if=$hudsonImage of=$selected_device status=progress conv=fsync >& /dev/null
printf "Connecting to New Brain\n"
mkdir -p /mnt/sd1343 &&
sleep 2

if mount ${selected_device}2 /mnt/sd1343; then
    echo "Connection success"
else
    echo "Error mount failed"
    exit 1
fi


hudsonValue="Hudson-"$(cat /dev/urandom | tr -dc 'A-Z0-9' | fold -w 4 | head -n 1)
hudsonPassword=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 12 | head -n 1)
printf "\n\nHudsons new name is: ${hudsonValue}\nPlease write this down\n\n"
printf "\n\nHudsons new password is: ${hudsonPassword}\nPlease write this down(It's case sensitive)\n\n"


perl -i -0pe "s{hudsonValue}{$hudsonValue}g" /mnt/sd1343/etc/wpa_supplicant/wpa_supplicant-wlan1.conf || { echo 'Failed to copy neccessary files to new OS' ; exit 1; }
perl -i -0pe "s{hudsonPassword}{$hudsonPassword}g" /mnt/sd1343/etc/wpa_supplicant/wpa_supplicant-wlan1.conf || { echo 'Failed to copy neccessary files to new OS' ; exit 1; }
perl -i -0pe "s{hudsonPassword}{$hudsonPassword}g" /mnt/sd1343/hudson/install/install-files/setup_mutual_tls.sh || { echo 'Failed to copy neccessary files to new OS' ; exit 1; }

eject $selected_device

echo "Nice. Take that SD out and plug it into your Brain."
